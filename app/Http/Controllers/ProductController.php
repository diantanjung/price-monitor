<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * param  \Illuminate\Http\Request  $request
     * return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //read link html
        $html = file_get_contents($request->link);
        $dom  = new \DOMDocument;
        @$dom->loadHTML($html);

        //get title
        $title_node = $dom->getElementsByTagName('h1');
        $title = trim($title_node[0]->nodeValue);

        //get price
        $finder = new \DomXPath($dom);
        $price_node = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' price ')]");
        $price = $price_node[0]->nodeValue;

        //get description
        $desc_node = $dom->getElementById('description');
        $description = trim($desc_node->nodeValue);

        $product                = new Product();
        $product->name          = $title;
        $product->price         = $price;
        $product->description   = $description;
        $product->save();

        return redirect('detail/'.$product->id);
    }

    public function list(){
        $products = Product::all();

        $data = [
            'products' => $products,
        ];

        return view('list', $data);
    }

    public function detail($id){
        $product = Product::findOrFail($id);

        $data = [
            'product' => $product,
        ];

        return view('detail', $data);
    }

    public function destroy($id){
        $product = Product::findOrFail($id);
        $product->delete();

        return redirect('list');
    }

}
