<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Page Three</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    </head>
    <body>
        <div class="container">
            <h2>Page Two</h2>
            <nav class="navbar navbar-inverse">
              <div class="container-fluid">
                <ul class="nav navbar-nav">
                  <li class="active"><a href="{{url('/')}}">Add link of Product</a></li>
                  <li><a href="{{url('list')}}">List of Products</a></li>
                </ul>
              </div>
            </nav>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Name</th>
                  <th scope="col">Price</th>
                  <th scope="col">Description</th>
                </tr>
              </thead>
              <tbody>
                @php $no=1; @endphp
                @foreach($products as $product)
                <tr>
                  <th scope="row">{{$no}}</th>
                  <td><a href="{{url('/detail/'.$product->id)}}">{{$product->name}}</a></td>
                  <td>{{$product->price}}</td>
                  <td>{{$product->description}}</td>
                </tr>
                @php $no++; @endphp
                @endforeach
              </tbody>
            </table>
        </div>
    </body>
</html>
