<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'index', 'uses' => 'ProductController@index']);
Route::get('/list', ['as' => 'list', 'uses' => 'ProductController@list']);
Route::post('/store', ['as' => 'store', 'uses' => 'ProductController@store']);
Route::get('/detail/{id_product}', ['as' => 'detail', 'uses' => 'ProductController@detail']);
Route::delete('destroy/{id_product}', ['as' => 'destroy', 'uses' => 'ProductController@destroy']);

